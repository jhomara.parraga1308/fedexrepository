<!-- footer section start -->
   <div class="footer_section layout_padding margin_top_90">
      <div class="container">
         <div class="newsletter_section">
            <div class="newsletter_left">
               <div class="footer_logo"><img src=" <?php echo base_url(); ?>/plantilla/images/logo.png"></div>
            </div>
            <div class="newsletter_right">
               <h5 class="newsletter_taital">Subscribe Newsletter</h5>
               <div class="subscribe_main">
                  <input type="text" class="mail_text" placeholder="Enter your email" name="text">
                  <div class="subscribe_bt"><a href="#">Subscribe</a></div>
               </div>
            </div>
         </div>
         <div class="footer_taital_main">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <h2 class="useful_text">About</h2>
                  <p class="ipsum_text">Fedex es un servicio de paquetería y servientrega a nivel internacional.</p>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h2 class="useful_text">Menus</h2>
                  <div class="footer_links">
                     <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about.html">Nosotros</a></li>
                        <li><a href="services.html">Sucursales</a></li>
                        <li><a href="pricing.html">Clientes</a></li>
                        <li><a href="contact.html">Contactos</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h2 class="useful_text">Useful Link</h2>
                  <div class="footer_links">
                     <ul>
                        <li><a href="#">Adipiscing</a></li>
                        <li><a href="#">Elit, sed do</a></li>
                        <li><a href="#">Eiusmod </a></li>
                        <li><a href="#">Tempor </a></li>
                        <li><a href="#">incididunt</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h2 class="useful_text">Contact us</h2>
                  <div class="addres_link">
                     <ul>
                        <li><a href="#"><img src="plantilla/images/map-icon.png"><span class="padding_left_10">No.123 Chalingt
                                 Gates</span></a></li>
                        <li><a href="#"><img src="plantilla/images/call-icon.png"><span class="padding_left_10">+01
                                 1234567890</span></a></li>
                        <li><a href="#"><img src="plantilla/images/mail-icon.png"><span
                                 class="padding_left_10">fedex@gmail.com</span></a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <h1 class="follow_text">Follow Us</h1>
            <div class="social_icon">
               <ul>
                  <li><a href="#"><img src="plantilla/images/fb-icon.png"></a></li>
                  <li><a href="#"><img src="plantilla/images/twitter-icon.png"></a></li>
                  <li><a href="#"><img src="plantilla/images/linkedin-icon.png"></a></li>
                  <li><a href="#"><img src="plantilla/images/instagram-icon.png"></a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- footer section end -->
   <!-- copyright section start -->
   <div class="copyright_section">
      <div class="container">
         <p class="copyright_text">Copyright 2019 All Right Reserved By.<a href="https://html.design"> Free html
               Templates</a> Distributed by. <a href="https://themewagon.com">ThemeWagon</a> </p>
         </p>
      </div>
   </div>
   <!-- copyright section end -->
   <!-- Javascript files-->
   <script src="plantilla/js/jquery.min.js"></script>
   <script src="plantilla/js/popper.min.js"></script>
   <script src="plantilla/js/bootstrap.bundle.min.js"></script>
   <script src="plantilla/js/jquery-3.0.0.min.js"></script>
   <script src="plantilla/js/plugin.js"></script>
   <!-- sidebar -->
   <script src="plantilla/js/jquery.mCustomScrollbar.concat.min.js"></script>
   <script src="plantilla/js/custom.js"></script>
   <!-- javascript -->
   <script src="plantilla/js/owl.carousel.js"></script>
   <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
   <script>
      $(document).ready(function () {
         $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
         });

   </script>
   <script>
            $(document).ready(function () {
               // Add minus icon for collapse element which is open by default
               $(".collapse.show").each(function () {
                  $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
               });

               // Toggle plus minus icon on show hide of collapse element
               $(".collapse").on('show.bs.collapse', function () {
                  $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
               }).on('hide.bs.collapse', function () {
                  $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
               });
            });
   </script>
</body>

</html>
